import {GLTFLoader} from 'three/examples/jsm/loaders/GLTFLoader';

export class GLTFArrayLoader {


    private standardLoader: any;

    private gltfObjectsMap: object;


    constructor() {
        this.standardLoader = new GLTFLoader();

        this.gltfObjectsMap = {};
    }


    public loadAssets(assetsMap, callback) {
        let me = this,
            assetIds = Object.keys(assetsMap),
            assetId,
            assetPath;

        console.log("Starting load of aseets: ", assetsMap);

        let loadAssetByIndex = (index) => {
            assetId = assetIds[index];
            assetPath = assetsMap[assetId];

            me.loadSingleAsset(assetPath, function (gltfObj) {
                me.gltfObjectsMap[assetId] = gltfObj;

                console.log("Have loaded asset id=%s, path=%s", assetId, assetPath);
                if (index >= assetIds.length - 1) {
                    callback(me.gltfObjectsMap);
                    return;
                }

                loadAssetByIndex(index + 1)
            });
        };


        loadAssetByIndex(0);
    }

    public loadSingleAsset(path, callback) {

        console.log("Load single asset call ", path);
        this.standardLoader.load(path, function (gltf) {
            if (callback) {
                console.log("Loaded single asset", path);
                callback(gltf);
                return;
            }
            throw new Error("Unhandled asset load callback.");
        })
    }

    public getModelById(modelId) {
        return this.gltfObjectsMap[modelId];
    }


}

