
export class ShadersLoader {

    private readonly resources: object;

    constructor(){
        this.resources = {};
    }

    public loadResources( config, callback ){
        let me = this,
            key, info,

            keys = Object.keys(config),

            i, url;

        if(keys.length === 0){
            callback(me.resources);
            return;
        }


        function loadSingleResource(url, cb){
            let request = new XMLHttpRequest();
            request.open('POST', url, true);
            request.addEventListener('load', function() {
                cb(request.responseText);
            });
            request.send();
        }

        i = 0;
        key = keys[i];
        info = config[keys[i]];
        url = info.url;
        let cbFunction = ( text )=>{
            me.resources[key] = text;
            i++;
            if (i >= keys.length) {
                if (callback) {
                    callback(me.resources);
                }
                return;
            }

            key = keys[i];
            info = config[keys[i]];
            url = info.url;
            loadSingleResource( url, cbFunction );
        };

        loadSingleResource( url, cbFunction );

    }


    public getResource( key ){
        return this.resources[key];
    }
}
