export enum ControlMode {
    RATE,
    ANGLE,
    LINEAR_VELOCITY
}