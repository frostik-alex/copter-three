import {ChartParams} from "../SimulationParams";

const BG_WIDTH = 1280;
const BG_HEIGHT = 720;


const TIME_FRAME = 2000;

const Y_SCALE = 70;


const DEFAULT_STYLE = {
    color: "black",
    lineWidth: 1
};

export class Charter {

    private bgCanvas: HTMLCanvasElement;
    private bgCtx: CanvasRenderingContext2D;


    private timeSeries: Array<number>;
    private valuesArray: Array<Array<number>>;
    private valueStylesArray: Array<object>;


    constructor() {
        this.bgCanvas = document.createElement("canvas");
        this.bgCanvas.width = BG_WIDTH;
        this.bgCanvas.height = BG_HEIGHT;

        this.bgCtx = this.bgCanvas.getContext("2d");

        this.timeSeries = [];
        this.valuesArray = [];
        this.valueStylesArray = [];
    }


    public addDataStyle(index: number, styleObj: object): void {
        this.valueStylesArray[index] = styleObj;
        this.valuesArray[index] = [];
    }


    private drawFrame(ctx: CanvasRenderingContext2D): void {
        ctx.save();

        ctx.strokeStyle = "black";
        ctx.lineWidth = 2;

        ctx.beginPath();
        ctx.rect(0, 0, BG_WIDTH, BG_HEIGHT);
        ctx.stroke();

        ctx.restore();
    }


    public render(ctx: CanvasRenderingContext2D, screenWidth: number, screenHeight: number): void {

        let x = ChartParams.CHART_X,
            y = screenHeight - ChartParams.CHART_HEIGHT - ChartParams.CHART_Y_BOT_OFFSET,
            w = ChartParams.CHART_WIDTH,
            h = ChartParams.CHART_HEIGHT;


        this.bgCtx.clearRect(0, 0, BG_WIDTH, BG_HEIGHT);

        this.drawFrame(this.bgCtx);

        for (let i = 0; i < this.valuesArray.length; i++) {
            let dataSeries: Array<number> = this.valuesArray[i];
            let dataStyle: object = this.valueStylesArray[i];

            this.renderDataSeries(dataSeries, this.timeSeries, dataStyle);
        }

        ctx.drawImage(this.bgCanvas, x, y, w, h);
    }


    private renderDataSeries(dataSeries: Array<number>, timeSeries: Array<number>, style: object): void {
        if (dataSeries.length !== timeSeries.length || dataSeries.length < 2) {
            return;
        }

        let ctx = this.bgCtx;

        let scaledDataSeries = [];
        let scaledTimeSeries = [];
        let dataSeriesStyle = style ? style : DEFAULT_STYLE;


        let firstTimeValue = timeSeries[0];


        for (let i = 0; i < dataSeries.length; i++) {

            let oldX = timeSeries[i];
            let oldY = dataSeries[i];

            let newX = ((oldX - firstTimeValue) / TIME_FRAME) * BG_WIDTH;
            let newY = (BG_HEIGHT - oldY * Y_SCALE) - BG_HEIGHT / 2;

            scaledDataSeries.push(newY);
            scaledTimeSeries.push(newX);
        }

        this.drawLineSeries(ctx, scaledTimeSeries, scaledDataSeries, dataSeriesStyle);
    }


    private drawLineSeries(ctx: CanvasRenderingContext2D, xArray: Array<number>, yArray: Array<number>, styleObj: object) {
        if (xArray.length !== yArray.length || xArray.length < 2) {
            return;
        }

        ctx.save();

        ctx.lineWidth = styleObj["lineWidth"];
        ctx.strokeStyle = styleObj["color"];

        ctx.beginPath();
        ctx.moveTo(xArray[0], yArray[0]);

        for (let i = 1; i < xArray.length; i++) {
            ctx.lineTo(xArray[i], yArray[i]);
        }

        ctx.stroke();

        ctx.restore();
    }

    public addValues(time: number, values: Array<number>): void {

        if (this.valuesArray.length !== values.length) {
            throw new Error("Wrong values array size provided for charter.");
        }

        //add latest values
        this.timeSeries.push(time);
        for (let i = 0; i < this.valuesArray.length; i++) {
            this.valuesArray[i].push(values[i]);
        }


        //remove all old records
        for (let i = 0; i < this.timeSeries.length; i++) {
            let timeRecord = this.timeSeries[i];

            //in case if time record is still in frame - do not delete anything
            if (time - timeRecord <= TIME_FRAME) {
                break;
            }

            //delete records that are not in "FRAME"
            this.timeSeries.shift();
            for (let i = 0; i < this.valuesArray.length; i++) {
                this.valuesArray[i].shift();
            }
        }
    }


}