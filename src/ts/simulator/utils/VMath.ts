import {Vec3d} from "./Vec3d";
import {Vec2d} from "./Vec2d";
import {Matrix} from "./Matrix";

const cos = Math.cos;
const sin = Math.sin;

export class VMath {


    static scaleVec(vec: Vec3d, scale: number, updateExisting?: boolean): Vec3d;
    static scaleVec(vec: Vec2d, scale: number, updateExisting?: boolean): Vec2d;
    static scaleVec(vec: any, scale: number, updateExisting?: boolean) {
        if (vec instanceof Vec2d) {

            if (!updateExisting) {
                return new Vec2d(vec.x * scale, vec.y * scale);
            }

            vec.x *= scale;
            vec.y *= scale;

            return vec;
        }

        if (!updateExisting) {
            return new Vec3d(vec.x * scale, vec.y * scale, vec.z * scale);
        }

        vec.x *= scale;
        vec.y *= scale;
        vec.z *= scale;

        return vec;
    }


    static normalize(vec: Vec3d, createNew?: boolean): Vec3d;
    static normalize(vec: Vec2d, createNew?: boolean): Vec2d;
    static normalize(vec: any, createNew?: boolean) {

        if (vec instanceof Vec2d) {
            let vecLength = vec.length;

            if(vecLength == 0){
                return createNew ? new Vec2d(0,0) : vec;
            }

            if (createNew) {
                return new Vec2d(vec.x / vecLength, vec.y / vecLength);
            }

            vec.x /= vecLength;
            vec.y /= vecLength;

            return vec;
        }

        let vecLength = vec.length;

        if(vecLength == 0){
            return createNew ? new Vec3d(0,0,0) : vec;
        }

        if (createNew) {
            return new Vec3d(vec.x / vecLength, vec.y / vecLength, vec.z / vecLength);
        }

        vec.x /= vecLength;
        vec.y /= vecLength;
        vec.z /= vecLength;

        return vec;

    }

    static vec2dSub(v1: Vec2d, v2: Vec2d): Vec2d {
        return new Vec2d(v1.x - v2.x, v1.y - v2.y);
    }

    static vec3dSub(v1: Vec3d, v2: Vec3d): Vec3d {
        return new Vec3d(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
    }


    static vec2dSum(...vectors: Vec2d[]): Vec2d {

        let resVec: Vec2d = new Vec2d(0,0);

        for(let i=0;i<vectors.length; i++){
            let vec = vectors[i];

            resVec.x += vec.x;
            resVec.y += vec.y;
        }
        return resVec;
    }

    static vec3dSum(...vectors: Vec3d[]): Vec3d {

        let resVec: Vec3d = new Vec3d(0,0,0);

        for(let i=0;i<vectors.length; i++){
            let vec = vectors[i];

            resVec.x += vec.x;
            resVec.y += vec.y;
            resVec.z += vec.z;
        }
        return resVec;
    }


    static vec2dMul(v1: Vec2d, v2: Vec2d): Vec2d {
        return new Vec2d(v1.x * v2.x, v1.y * v2.y)
    }


    static getDirVec2d(from: Vec2d, to: Vec2d): Vec2d {
        return this.normalize(VMath.vec2dSub(to, from));
    }


    static newEmptyVec2d(): Vec2d {
        return new Vec2d(0, 0);
    }


    static mulVec2dNum(vec: Vec2d, scalar: number): Vec2d {
        return new Vec2d(vec.x * scalar, vec.y * scalar);
    }


    static rotateVec2d(vec: Vec2d, angle: number): Vec2d {
        return new Vec2d(
            vec.x * cos(angle) - vec.y * sin(angle),
            vec.x * sin(angle) + vec.y * cos(angle)
        );
    }


    static numSign(x: number): number {
        return (Number(x > 0) - Number(x < 0)) || +x;
    }


    static numRatio(value: number, lowerBound: number, upperBound: number): number {
        return (value - lowerBound) / (upperBound - lowerBound);
    }


    static rotateVec3dX(vec: Vec3d, angle: number): Vec3d {
        return new Vec3d(
            vec.x,
            vec.y * cos(angle) - vec.z * sin(angle),
            vec.y * sin(angle) + vec.z * cos(angle)
        );
    }


    static rotateVec3dY(vec: Vec3d, angle: number): Vec3d {
        return new Vec3d(
            vec.x * cos(angle) + vec.z * sin(angle),
            vec.y,
            -vec.x * sin(angle) + vec.z * cos(angle)
        );
    }


    static rotateVec3dZ(vec: Vec3d, angle: number): Vec3d {
        return new Vec3d(
            vec.x * cos(angle) - vec.y * sin(angle),
            vec.x * sin(angle) + vec.y * cos(angle),
            vec.z
        );
    }


    static rotateVec3dAxis(vec: Vec3d, axis: Vec3d, angle: number): Vec3d {
        let sinV: number = sin(angle);
        let cosV: number = cos(angle);

        let ux: number = axis.x;
        let uy: number = axis.y;
        let uz: number = axis.z;

        return new Vec3d(
            vec.x * (cosV + ux * ux * (1 - cosV)) + vec.y * (ux * uy * (1 - cosV) - uz * sinV) + vec.z * (ux * uz * (1 - cosV) + uy * sinV),
            vec.x * (uy * ux * (1 - cosV) + uz * sinV) + vec.y * (cosV + uy * uy * (1 - cosV)) + vec.z * (uy * uz * (1 - cosV) - ux * sinV),
            vec.x * (uz * ux * (1 - cosV) - uy * (sinV)) + vec.y * (uz * uy * (1 - cosV) + ux * sinV) + vec.z * (cosV + uz * uz * (1 - cosV))
        );
    }

    static getRotXMat3d(angle: number) {
        let sinV: number = sin(angle);
        let cosV: number = cos(angle);

        let res: Matrix = Matrix.createMat3d();

        res.set([
            [1, 0, 0],
            [0, cosV, -sinV],
            [0, sinV, cosV]
        ]);

        return res;
    }

    static getRotYMat3d(angle: number) {
        let sinV: number = sin(angle);
        let cosV: number = cos(angle);

        let res: Matrix = Matrix.createMat3d();

        res.set([
            [cosV, 0, sinV],
            [0, 1, 0],
            [-sinV, 0, cosV]
        ]);

        return res;
    }

    static getRotZMat3d(angle: number) {
        let sinV: number = sin(angle);
        let cosV: number = cos(angle);

        let res: Matrix = Matrix.createMat3d();

        res.set([
            [cosV, -sinV, 0],
            [sinV, cosV, 0],
            [0, 0, 1]
        ]);

        return res;
    }

    static crossProduct3d(v1, v2: Vec3d): Vec3d {
        return new Vec3d(
            v1.y * v2.z - v1.z * v2.y,
            v1.z * v2.x - v1.x * v2.z,
            v1.x * v2.y - v1.y * v2.x
        );
    }

    static dotProduct3d(v1, v2: Vec3d): number {
        return v1.x * v2.x + v1.x * v2.y + v1.z * v2.z
    }

    static dotProduct2d(v1, v2: Vec2d) : number{
        return v1.x * v2.x + v1.y + v2.y;
    }

    static getAngleBetween3d(v1: Vec3d, v2: Vec3d): number{
        return Math.acos(VMath.dotProduct3d(v1,v2) / (v1.length * v2.length));
    }

    static getAngleBetween2d(v1: Vec2d, v2: Vec2d): number{
        return Math.acos(VMath.dotProduct2d(v1,v2) / (v1.length * v2.length));
    }

    static circularBound(value: number, lowerLimit: number, upperLimit: number): number {
        let normRange: number = upperLimit - lowerLimit,
            normValue = value - lowerLimit;

        normValue += normRange * (normValue > normRange ? -1 : ( normValue < 0 ? 1 : 0 ));

        return lowerLimit + normValue
    }


    static constrainValue(value: number, min: number, max: number): number {
        return Math.max(min, Math.min(value, max));
    }


    static radToDeg(radAngle: number): number {
        return radAngle / Math.PI * 180;
    }

    static degToRad(degAngle: number): number {
        return degAngle / 180 * Math.PI;
    }


    static mapValue(refValue, refMin, refMax, mapMin, mapMax: number): number {
        return mapMin + VMath.numRatio(refValue, refMin, refMax) * (mapMax - mapMin);
    }


    static mulMat(m1, m2: Matrix) {
        if (m1.getCols() != m2.getRows()) {
            throw new Error("Cols count of 1st Matrix doesn't correspond to number of rows in 2nd Matrix");
        }

        let result = new Matrix(m1.getRows(), m2.getCols());

        for (let i = 0; i < result.getRows(); i++) {
            for (let j = 0; j < result.getCols(); j++) {
                let element = 0;
                for (let k = 0; k < m1.getCols(); k++) {
                    element += m1[i][k] * m2[k][j];
                }
                result[i][j] = element;
            }

        }

        return result;
    }


    static mulVecMat(vec: Vec3d, mat: Matrix): Vec3d {

        return new Vec3d(
            mat[0][0] * vec.x + mat[0][1] * vec.y + mat[0][2] * vec.z,
            mat[1][0] * vec.x + mat[1][1] * vec.y + mat[1][2] * vec.z,
            mat[2][0] * vec.x + mat[2][1] * vec.y + mat[2][2] * vec.z
        );
    }

    static detMat2d(m: Matrix):number{
        if(m.getRows() != m.getCols()){
            throw new Error("Can't calculate determinant of non-square matrix");
        }

        if(m.getRows() != 2){
            throw new Error("2nd order matrix required.");
        }

        return  m[0][0]*m[1][1] - m[0][1]*m[1][0];
    }


    static detMat3d(m: Matrix):number{
        if(m.getRows() != m.getCols()){
            throw new Error("Can't calculate determinant of non-square matrix");
        }

        if(m.getRows() != 3){
            throw new Error("3rd order matrix required.");
        }

        return  m[0][0] * (m[1][1] * m[2][2] - m[1][2] * m[2][1]) -
                m[0][1] * (m[1][0] * m[2][2] - m[1][2] * m[2][0]) +
                m[0][2] * (m[1][0] * m[1][2] - m[1][1] * m[0][2]);
    }

    static getMinor(m: Matrix, row:number, col:number): Matrix{

        let resMat = new Matrix(m.getRows()-1, m.getCols()-1, true);

        for(let i=0; i<m.getRows(); i++){

            if(i == row){
                continue;
            }

            let minorRowIndex = i < row ? i : i - 1;

            for(let j=0; j<m.getCols(); j++){

                if(j == col){
                    continue;
                }

                let minorColIndex = j < col ? j : j - 1;

                resMat[minorRowIndex][minorColIndex] = m[i][j];
            }
        }

        return resMat;
    }


    static determinant(m: Matrix):number{
        if(m.getRows() != m.getCols()){
            throw new Error("Can't calculate determinant of non-square matrix");
        }

        let res:number = 0;

        if(m.getRows() == 2){
            return VMath.detMat2d(m);
        }

        for(let i=0; i<m.getCols(); i++){
            res += Math.pow(-1,i+2) * m[0][i] * VMath.determinant( VMath.getMinor(m, 0, i) );
        }

        return res;
    }


    static matInverse(m: Matrix):Matrix{
        let det = VMath.determinant(m);

        if(det == 0){
            throw new Error("Zero determinant when calculating inverse.");
        }

        let inv = new Matrix(m.getRows(), m.getCols(), true);

        for(let i=0; i<inv.getRows(); i++){
            for(let j=0; j<inv.getCols(); j++){
                inv[i][j] = Math.pow(-1, i+j) * VMath.determinant( VMath.getMinor(m, j, i) );
            }
        }

        return inv.mul(1/det);
    }



    static mix(a:number, b: number, mixWeight: number):number{
        return a * (1.0 - mixWeight) + b * mixWeight;
    }
}
