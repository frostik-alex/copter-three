import {Object3D} from "three";
import {Vec2d} from "../utils/Vec2d";

export class Entity{

    public position : Vec2d;

    public visualObj : Object3D;

    constructor(startX : number, startY: number){
       this.position = new Vec2d(startX, startY);
    }

    public update(dt: number){}
};

