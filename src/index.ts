// add styles
import "./style.css";
// three.js
import * as THREE from "three";

(window as any).THREE = THREE;
import {Euler, Material, PerspectiveCamera, Scene, TextureLoader, Vector3} from "three";
import {ThreeEngine} from "./ts/simulator/ThreeEngine";
import {OrbitControls} from 'three/examples/jsm/controls/OrbitControls';
import {DEG2RAD, degToRad} from 'three/src/math/MathUtils';
import {GLTFArrayLoader} from './ts/utils/GLTFArrayLoader';
import {ShadersLoader} from './ts/utils/ShadersLoader';


const GLTF_CONFIG = {
    "drone": "src/models/drone/scene.gltf",
    // "drone_highres" : "src/models/drone_skeletal_mesh/scene.gltf"
};

const SHADERS_CONFIG = {
    // "serega_totoshka": {url: "src/models/serega/serega_totoshka3.png"}
};



const RESOURCES_LOADER = new ShadersLoader();
const GLTF_ARRAY_LOADER = new GLTFArrayLoader();

const SKY_BOX_PATH = 'src/models/skyboxsun25deg/';
const SKY_BOX_IMAGES_FORMAT = '.jpg';
const SKY_BOX_LOADER = new THREE.CubeTextureLoader();


function initScene(envMap, resourcesMap, gltfMap) {

    let clock = new THREE.Clock();


    let scene = new Scene();
    let camera = new PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

    let engine = new ThreeEngine(scene, camera) ;




// add axis to the scene
    let axis = new THREE.AxesHelper(10);
    scene.add(axis);

// add lights
    let light = new THREE.DirectionalLight(0xffffff, 1.0);
    light.position.set(100, 100, 100);
    scene.add(light);

    let light2 = new THREE.DirectionalLight(0xffffff, 1.0);
    light2.position.set(0, 100, 0);
    scene.add(light2);
//

    let ambientLight = new THREE.AmbientLight(0xffffff, 1.0);
    scene.add(ambientLight);

    var controls = new OrbitControls(camera, engine.screenCanvas);


    scene.background = envMap;

    camera.position.x = 1;
    camera.position.y = 1;
    camera.position.z = 1;

    camera.lookAt(scene.position);


    let droneScene: THREE.Scene = GLTF_ARRAY_LOADER.getModelById("drone").scene;
    droneScene.children[0].rotation.z = -45.0 * DEG2RAD;
    console.log("DRONEE SXENE", droneScene);

    droneScene.traverse(function (child: any) {
        if (child.isMesh) {
            child.material.envMap = envMap;
        }
    });


    // let highResDroneScene: THREE.Scene = GLTF_ARRAY_LOADER.getModelById("drone_highres").scene;
    // highResDroneScene.children[0].rotation.z = -180.0 * DEG2RAD;
    // highResDroneScene.scale.set(0.005, 0.005, 0.005);
    // highResDroneScene.traverse(function (child: any) {
    //     if (child.isMesh) {
    //         child.material.envMap = envMap;
    //     }
    // });

    // (window as any).drone = droneScene.children[0];
    // (window as any).propellers = [];
    // console.log("DRONE PROPELLER", droneScene.children[0].children[0].children[0].children[102] );

    scene.add(droneScene);


    engine.copter.setMesh(droneScene);


    let geometry = new THREE.PlaneBufferGeometry(100, 100, 1, 1);
    geometry.rotateX(degToRad(-90));


    let seregaTexture = new TextureLoader().load("src/models/serega/serega_totoshka3.png");
    let terrainMaterial: Material = new THREE.MeshBasicMaterial({map: seregaTexture});
    let terrainMesh = new THREE.Mesh(geometry, terrainMaterial);
    terrainMesh.position.y = -0.07;
    scene.add(terrainMesh);


    function animate(): void {
        requestAnimationFrame(animate);
        render();
    }

    function render(): void {

        let dt = clock.getDelta();
        if(!dt) return;

        engine.update(dt);


        let copterPosition = droneScene.position.clone();


        // droneScene.children[0].children[0].children[0].children[102].rotation.y += 0.01;
        // droneScene.children[0].children[0].children[0].children[103].rotation.y += 0.01;
        // droneScene.children[0].children[0].children[0].children[104].rotation.y += 0.01;
        // droneScene.children[0].children[0].children[0].children[105].rotation.y += 0.01;


        let propellers = (window as any).propellers;


        // propellers[0].rotation.y += 0.01;
        // propellers[0].rotation.y += 0.01;



        // for(let i=0; i<propellers.length; i++){
        //     let singlePropeller = propellers[i];
        //
        //     singlePropeller.rotation.y += 0.01;
        // }

        let backwardsVector = new Vector3(-1.5, 0.75, 0);
        let rotation = new Euler(0,  -engine.copter.getSensorAngularPos().z, 0);
        backwardsVector.applyEuler(rotation);
        // backwardsVector.applyQuaternion(droneScene.quaternion);

        backwardsVector.add(copterPosition);

        controls.object.position.set(backwardsVector.x, backwardsVector.y, backwardsVector.z);
        controls.target.set(copterPosition.x, copterPosition.y, copterPosition.z );


        controls.update();

        engine.render(dt);
    }

    animate();
}


SKY_BOX_LOADER.load(
    [
        SKY_BOX_PATH + 'px' + SKY_BOX_IMAGES_FORMAT, SKY_BOX_PATH + 'nx' + SKY_BOX_IMAGES_FORMAT,
        SKY_BOX_PATH + 'py' + SKY_BOX_IMAGES_FORMAT, SKY_BOX_PATH + 'ny' + SKY_BOX_IMAGES_FORMAT,
        SKY_BOX_PATH + 'pz' + SKY_BOX_IMAGES_FORMAT, SKY_BOX_PATH + 'nz' + SKY_BOX_IMAGES_FORMAT
    ],
    (envMap) => {
        RESOURCES_LOADER.loadResources(SHADERS_CONFIG, (resources) => {
            GLTF_ARRAY_LOADER.loadAssets(GLTF_CONFIG, (gltfObjectsMap) => {
                initScene(envMap, resources, gltfObjectsMap);
            });
        });
    }
);





